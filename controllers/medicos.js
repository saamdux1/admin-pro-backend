const { response } = require('express');

const Medico = require('../models/medico');

const getMedicos = async(req, res = response)=>{
    const medicos = await Medico.find()
                                .populate('usuario','nombre img')
                                .populate('hospital','nombre img');
                                
    res.json({
        ok:true,
        medicos
    });
}

const crearMedico = async(req, res = response)=>{

    const uid = req.uid;
    const medico = new Medico({usuario:uid, ...req.body});

    try {

        const medicoDB = await medico.save();

        res.json({
            ok:true,
            medico: medicoDB
        });
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador no se pudo insertar médico'
        });
    }
}


const actualizarMedico = async(req, res = response)=>{

    const medicoId = req.params.id;
    const uid = req.uid;

    try {
        const medico = await Medico.findById(medicoId);

        if(!medico){
            res.status(404).json({
                ok:false,
                msg:'Médico no encontrado'
            });
        }

        const cambiosMedico = {
            ...req.body,
            usuario: uid
        }

        const medicoActualizado = await Medico.findByIdAndUpdate(medicoId, cambiosMedico, {new:true});

        res.json({
            ok:true,
            msg:'Médico Actualizado!!!',
            hospital: medicoActualizado
        });
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok:false,
            msg:'El Médico no se pudo actualizar, contacte al administrador'
        });
    }
}

const borrarMedico = async(req, res = response)=>{
    const medicoId = req.params.id;

    try {
        const medico = await Medico.findById(medicoId);

        if(!medico){
            res.status(404).json({
                ok:false,
                msg:'Médico no encontrado'
            });
        }

        await Medico.findByIdAndDelete(medicoId);

        res.json({
            ok:true,
            msg:'Médico Eliminado'
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok:false,
            msg:'El Médico no se pudo eliminar, contacte al administrador'
        });
    }
}


module.exports={
    getMedicos,
    crearMedico,
    actualizarMedico,
    borrarMedico
}